package UIE.sampad.control;

import UIE.sampad.control.common.AlreadyExistException;
import UIE.sampad.control.common.EntityNotFoundException;
import UIE.sampad.control.company.CompanyController;
import UIE.sampad.model.company.Company;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.EntityModel;

import java.util.LinkedList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CompanyControllerTest {
    @Autowired
    CompanyController controller;


    void addSomeCompanyForTest(){
        controller.addCompany(new Company("lule gostar sepahan","lule gostar","123","luleGostar@gmail.com",null));
    }


    @Test
    void TestAddCompany() {
        assertThat(controller).isNotNull();
        assertNotNull(controller);
        Company company=new Company("yonjeh gostar margh","Yonjeh gostar","1234","YonjehGostar@gmail.com","tahiye khorak dam");
        controller.addCompany(company);
        Optional<Company> addedCompany=controller.getRepository().findById("Yonjeh gostar");
        addedCompany.ifPresent(value -> assertEquals(company, value));
    }

    @Test
    void TestAlreadyException(){
        //addSomeCompanyForTest();
        assertThrows(AlreadyExistException.class,() ->controller.addCompany(new Company("lule gostar sepahan","lule gostar","123","luleGostar@gmail.com",null)));
    }

    @Test
    void TestGetCompanyById() {
        addSomeCompanyForTest();
        assertEquals(controller.getCompanyById("lule gostar").getContent(),new Company("lule gostar sepahan","lule gostar","123","luleGostar@gmail.com",null));
        try {
            controller.getCompanyById("something that hasnt been added to the database");
        }
        catch (Exception exception) {
            assertTrue(exception instanceof EntityNotFoundException);
            assertEquals(exception.getMessage(),"couldn't find the company with id:"+"something that hasnt been added to the database");
        }
    }

    @Test
    void TestSearchCompanyById() {
        //addSomeCompanyForTest();
        controller.addCompany(new Company("lol","lol","lol","lol",null));
        LinkedList<Company> expectedCompanies=new LinkedList<>();
        expectedCompanies.add(new Company("lule gostar sepahan","lule gostar","123","luleGostar@gmail.com",null));
        expectedCompanies.add(new Company("lol","lol","lol","lol",null));
        LinkedList<Company> foundCompanies=new LinkedList<>();
        for(EntityModel<Company> companyModel:controller.searchCompaniesById("l")) {
            foundCompanies.add(companyModel.getContent());
        }

        assertEquals(foundCompanies,expectedCompanies);

        try {
            controller.searchCompaniesById("some id that nothing like it hasn been added");
        } catch (Exception exception) {
            assertTrue(exception instanceof EntityNotFoundException);
            assertEquals(exception.getMessage(),"couldn't find the company with id:"+"some id that nothing like it hasn been added");
        }
    }

}