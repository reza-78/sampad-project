package UIE.sampad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampadApplication.class, args);
	}

}
