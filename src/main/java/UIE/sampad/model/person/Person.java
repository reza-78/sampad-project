package UIE.sampad.model.person;

import UIE.sampad.model.payment.Payment;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Person {

    private String name;
    @Id @GeneratedValue
    private long id;
    @Setter
    private String email;
    @Setter
    private String phoneNumber;
    @OneToMany(mappedBy = "person")
    List<Payment> payments = new ArrayList<>();
    //may be changed

    public Person(String name, String email, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;

    }

    public void addPayment(Payment payment) {
        payments.add(payment);
    }
    // todo write a method for deleting payments


}




