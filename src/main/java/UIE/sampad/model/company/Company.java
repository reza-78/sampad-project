package UIE.sampad.model.company;

import UIE.sampad.model.payment.Payment;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "company")
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Company {
    @Setter
    private String name;
    @Id @GeneratedValue
    private long id;
    @Setter
    private String userName;
    @Setter
    private String password;
    @Setter
    private String email;
    @Setter
    private String description;

    @OneToMany(mappedBy = "company")
    @EqualsAndHashCode.Exclude
    private List<Payment> payments = new ArrayList<>();
    //needs change and fix

    public Company(String name, String userName, String password, String email, String description) {
        this.name = name;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.description = description;
    }

    public void addPayment(Payment payment){
        payments.add(payment);
    }

    public Payment getPaymentById(Long personId){
        for(Payment payment:payments){
            if (payment.getPaymentKey().getPersonId().equals(personId))
                return payment;
        }
        return null;
    }

}
