package UIE.sampad.model.payment;


import UIE.sampad.model.company.Company;
import UIE.sampad.model.person.Person;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Payment {
    @Setter
    @EmbeddedId
    private PaymentKeyholder paymentKey;
    @ManyToOne
    @MapsId("companyId")
    //can not resolve the column error
    //"Unresolved database references in annotations" and uncheck it.
    //we can add PostgreSQl as data resource later
    @JoinColumn(name = "companyId")
    private Company company;

    @ManyToOne
    @MapsId("personId")
    @JoinColumn(name = "personId")
    private Person person;
    @Setter
    private double donationMoney;
    @Setter
    private int period; //its in days
    private Date startDate;//the day that donation schedule starts
    private double debt;
    public void updateDebt(double change)/*gets a change and applies it to the debt*/ {
        this.debt+=change;
    }

}
