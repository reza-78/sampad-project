package UIE.sampad.model.payment;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@Embeddable
public class PaymentKeyholder implements Serializable {
    @Column(name = "companyId")
    String companyId;

    @Column(name = "personId")
    String personId;

}


