package UIE.sampad.model.transaction;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@ToString
@Getter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class TransactionKeyholder implements Serializable {
    @Column(name = "companyId")
    String companyId;
    @Column(name = "personId")
    String personId;
}
