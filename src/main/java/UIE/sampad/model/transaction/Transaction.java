package UIE.sampad.model.transaction;

import UIE.sampad.model.company.Company;
import UIE.sampad.model.person.Person;
import lombok.*;

import javax.persistence.*;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Transaction {
    @Setter
    @EmbeddedId
    private TransactionKeyholder transactionKey;

    @ManyToOne
    @MapsId("companyId")
    @JoinColumn(name = "companyId")
    private Company receiver;

    //can not resolve the column error
    //"Unresolved database references in annotations" and uncheck it.
    //we can add PostgreSQl as data resource later

    @ManyToOne
    @MapsId("personId")
    @JoinColumn(name = "personId")
    private Person payer;

    @Setter
    private long amount;

}
