package UIE.sampad.control.person;

import UIE.sampad.control.common.AlreadyExistException;
import UIE.sampad.control.common.EntityNotFoundException;
import UIE.sampad.control.common.InvalidInputException;
import UIE.sampad.model.person.Person;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@AllArgsConstructor
@RestController
public class PersonController {
    PersonRepository repository;
    PersonModelAssembler assembler;

    @PostMapping("/people")
    ResponseEntity<?> addPerson(@RequestBody Person newPerson){
        if (newPerson.getEmail()==null && newPerson.getPhoneNumber()==null) {
            throw new InvalidInputException("id");
        }
//        newPerson.setId(newPerson.getEmail(),newPerson.getPhoneNumber());
//        String newPersonId = newPerson.getId();
//       if (repository.findById(newPersonId).isPresent())
//            throw new AlreadyExistException(newPersonId,"person");
        EntityModel<Person> entityModel = assembler.toModel(repository.save(newPerson));
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel );
    }
    @GetMapping("/people/{id}")
    EntityModel<Person> getPersonById(@PathVariable Long id){
        Optional<Person> foundPerson = repository.findById(id);
        if (foundPerson.isPresent())
            return assembler.toModel(foundPerson.get()  );
        throw new EntityNotFoundException(""+id , "person");
    }
    @PutMapping("/people/{id}")
    ResponseEntity<?> update(@RequestBody Person newPerson , @PathVariable Long id){
        Optional<Person> updatePerson = repository.findById(id).map(person -> {
            String newPersonEmail=newPerson.getEmail();
            String newPersonPhoneNumber = newPerson.getPhoneNumber();
            if (newPersonEmail!=null)
                person.setEmail(newPersonEmail);
            if (newPersonPhoneNumber!=null)
                person.setPhoneNumber(newPersonPhoneNumber);
            return repository.save(person);
        })/*TODO: I removed the or else get and I have no idea what that can do here because ids cant collide anymore*/;
        EntityModel<Person> entityModel = assembler.toModel(updatePerson.get());
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @DeleteMapping("/people/{id}")
    ResponseEntity<?> delete(@PathVariable Long id){
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        throw new EntityNotFoundException(""+id,"person");
    }
}
