package UIE.sampad.control.payment;

import UIE.sampad.model.payment.Payment;
import UIE.sampad.model.payment.PaymentKeyholder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment,PaymentKeyholder> {
}
