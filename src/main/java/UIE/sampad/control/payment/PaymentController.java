package UIE.sampad.control.payment;

import UIE.sampad.control.common.AlreadyExistException;
import UIE.sampad.control.company.CompanyRepository;
import UIE.sampad.control.common.EntityNotFoundException;
import UIE.sampad.control.common.InvalidInputException;
import UIE.sampad.control.person.PersonRepository;
import UIE.sampad.model.company.Company;
import UIE.sampad.model.payment.Payment;
import UIE.sampad.model.payment.PaymentKeyholder;
import UIE.sampad.model.person.Person;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Optional;

@RestController
public class PaymentController {
    CompanyRepository companyRepository;
    PersonRepository personRepository;
    PaymentRepository repository;
    PaymentModelAssembler assembler;
//    EntityManagerFactory companyManagerFactory = Persistence.createEntityManagerFactory("Company");
//    EntityManager companyEntityManager = companyManagerFactory.createEntityManager();
//
//    EntityManagerFactory personManagerFactory = Persistence.createEntityManagerFactory("Person");
//    EntityManager personEntityManager = personManagerFactory.createEntityManager();

    public PaymentController(CompanyRepository companyRepository, PersonRepository personRepository ,PaymentRepository repository , PaymentModelAssembler assembler) {
        this.companyRepository = companyRepository;
        this.personRepository = personRepository;
        this.assembler = assembler;
        this.repository=repository;
    }

    @PostMapping("/payments")
    ResponseEntity<?> addPayment(@RequestBody Payment newPayment){
        PaymentKeyholder paymentId = newPayment.getPaymentKey();
        if (paymentId==null)
            throw new InvalidInputException("Payment id");
        if (repository.findById(paymentId).isPresent())
            throw new AlreadyExistException(paymentId.toString(),"Payment");
        EntityModel<Payment> entityModel=assembler.toModel(newPayment);

//        String companyId = newPayment.getPaymentKey().getCompanyId();
//        String personId = newPayment.getPaymentKey().getPersonId();
//        Company company= companyEntityManager.find(Company.class,companyId);
//        Person person = personEntityManager.find(Person.class,personId);
//        company.addPayment(newPayment);
//        person.addPayment(newPayment);

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @GetMapping("/payments/{id}")
    EntityModel<Payment> getPaymentById(@PathVariable PaymentKeyholder id){
        Optional<Payment> foundPayment = repository.findById(id);
        if (foundPayment.isPresent())
            return assembler.toModel(foundPayment.get());
        throw new EntityNotFoundException(id.toString(),"payment");
    }

    @PutMapping("/payments/{id}")
    ResponseEntity<?> update(@RequestBody Payment newPayment,@PathVariable PaymentKeyholder id){
        Payment updatePayment = repository.findById(id).map(payment -> {
            payment.setDonationMoney(newPayment.getDonationMoney());
            payment.setPeriod(newPayment.getPeriod());
            return repository.save(payment);
        }).orElseGet(() -> {
            newPayment.setPaymentKey(id);
            return repository.save(newPayment);
        });
        EntityModel<Payment> entityModel = assembler.toModel(updatePayment);
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @DeleteMapping("/payments/{id}")
    ResponseEntity<?> delete(@PathVariable PaymentKeyholder id){
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        throw new EntityNotFoundException(id.toString(),"company");
    }
}
