package UIE.sampad.control.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String id , String entityType) {
        super("couldn't find the "+ entityType +" with id:"+id);
    }
}

@ControllerAdvice
class EntityNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ResponseEntity<ApiError> employeeNotFoundHandler(EntityNotFoundException exception) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND,exception.getMessage());
        return new ResponseEntity<>(apiError,HttpStatus.NOT_FOUND);
        //we can add  HttpHeaders() in ResponseEntity arguments
    }
}
