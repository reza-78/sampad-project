package UIE.sampad.control.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class InvalidInputException extends RuntimeException{
    public InvalidInputException(String invalidField){
        super("your input : " + invalidField + " is invalid");
    }
}

@ControllerAdvice
class InvalidInputAdvice{
    @ResponseBody
    @ExceptionHandler(InvalidInputException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    ResponseEntity<ApiError> invalidInputHandler(InvalidInputException exception){
        ApiError apiError= new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,exception.getMessage());
        return new ResponseEntity<>(apiError,HttpStatus.UNPROCESSABLE_ENTITY);
        //we can add  HttpHeaders() in ResponseEntity arguments
    }
}
