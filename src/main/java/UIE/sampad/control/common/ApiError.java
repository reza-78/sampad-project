package UIE.sampad.control.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public class ApiError {
    private final HttpStatus httpStatus;
    private final String message;
}
