package UIE.sampad.control.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class AlreadyExistException extends RuntimeException{
    public AlreadyExistException(String id ,String entityType){
        super(entityType+" with id " +id + " already exists");
    }
}

@ControllerAdvice
class AlreadyExistAdvice{
    @ResponseBody
    @ExceptionHandler(AlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    ResponseEntity<ApiError> companyNotFoundHandler(AlreadyExistException exception){
        ApiError apiError = new ApiError(HttpStatus.CONFLICT,exception.getMessage());
        return new ResponseEntity<>(apiError,HttpStatus.CONFLICT);
        //we can add  HttpHeaders() in ResponseEntity arguments
    }
}