package UIE.sampad.control.transaction;

import UIE.sampad.control.common.AlreadyExistException;
import UIE.sampad.control.common.EntityNotFoundException;
import UIE.sampad.control.common.InvalidInputException;
import UIE.sampad.model.transaction.Transaction;
import UIE.sampad.model.transaction.TransactionKeyholder;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@AllArgsConstructor
@RestController
public class TransactionController {
    TransactionRepository repository;
    TransactionAssembler assembler;

    @GetMapping("/transactions/{id}")
    EntityModel<Transaction> getTransactionById(@PathVariable TransactionKeyholder id){
        Optional<Transaction> foundTransaction = repository.findById(id);
        if (foundTransaction.isPresent())
            return assembler.toModel(foundTransaction.get());
        throw new EntityNotFoundException(id.toString(),"transaction");
    }

    @PutMapping("/transactions/{id}")
    ResponseEntity<?> update(@RequestBody Transaction newTransaction,@PathVariable TransactionKeyholder id){
        Transaction updateTransaction = repository.findById(id).map(transaction -> {
            transaction.setAmount(newTransaction.getAmount());
            return repository.save(transaction);
        }).orElseGet(() ->{
            newTransaction.setTransactionKey(id);
            return repository.save(newTransaction);
        });
        EntityModel<Transaction> entityModel = assembler.toModel(updateTransaction);
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @PostMapping("/transactions")
    ResponseEntity<?> addTransaction(@RequestBody Transaction newTransaction){
        TransactionKeyholder transactionKey = newTransaction.getTransactionKey();
        if (transactionKey==null)
            throw new InvalidInputException("transaction id");
        if(repository.findById(transactionKey).isPresent())
            throw new AlreadyExistException(transactionKey.toString(),"transaction");
        EntityModel<Transaction> entityModel = assembler.toModel(newTransaction);
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);

    }

    @DeleteMapping("/transactions/{id}")
    ResponseEntity<?> delete(@PathVariable TransactionKeyholder id){
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        throw new EntityNotFoundException(id.toString(),"transaction");
    }
}
