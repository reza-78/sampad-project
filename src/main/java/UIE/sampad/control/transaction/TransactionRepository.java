package UIE.sampad.control.transaction;

import UIE.sampad.model.transaction.Transaction;
import UIE.sampad.model.transaction.TransactionKeyholder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, TransactionKeyholder> {
}
