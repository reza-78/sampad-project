package UIE.sampad.control.company;

import UIE.sampad.model.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company,Long> {
    List<Company> findAllByUserNameContaining(String userName);
    Company findByUserNameMatches(String userName);
}