package UIE.sampad.control.company;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import UIE.sampad.model.company.Company;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class CompanyModelAssembler implements RepresentationModelAssembler<Company, EntityModel<Company>> {
    @Override
    public EntityModel<Company> toModel(Company company) {
        return EntityModel.of(company,linkTo(methodOn(CompanyController.class).getCompanyById(company.getId())).withSelfRel());
    }
}
