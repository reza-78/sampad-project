package UIE.sampad.control.company;

import UIE.sampad.control.common.AlreadyExistException;
import UIE.sampad.control.common.EntityNotFoundException;
import UIE.sampad.control.common.InvalidInputException;
import UIE.sampad.model.company.Company;
import UIE.sampad.model.payment.Payment;
import lombok.Getter;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.util.resources.ga.LocaleNames_ga;

import java.util.*;

@RestController
public class CompanyController {
    @Getter
    private final CompanyRepository repository;
    private final CompanyModelAssembler assembler;

    public CompanyController(CompanyRepository repository ,CompanyModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }
    @PostMapping("/companies")
    public ResponseEntity<?> addCompany(@RequestBody Company newCompany){
        String newCompanyUserName = newCompany.getUserName();
        if (newCompanyUserName==null)
            throw new InvalidInputException("id");
        if (repository.findByUserNameMatches(newCompanyUserName)!=null)
            throw new AlreadyExistException(newCompanyUserName ,"company");
        // todo input validation check
        EntityModel<Company> entityModel = assembler.toModel(repository.save(newCompany));
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @GetMapping("/companies/{id}")
    public EntityModel<Company> getCompanyById(@PathVariable Long id) {
        Optional<Company> foundCompany=repository.findById(id);
        if(foundCompany.isPresent()) {
            return assembler.toModel(foundCompany.get());        }
        throw new EntityNotFoundException(""+id,"company");
    }

    @GetMapping("/companies/search/userName/{userName}")
    public List<EntityModel<Company>>searchCompaniesByUserName(@PathVariable String userName) {
        List<Company> foundCompanies=repository.findAllByUserNameContaining(userName);
        List<EntityModel<Company>> foundCompanyEntities = new ArrayList<>();
        if (foundCompanies.size()!=0)
            foundCompanies.forEach((company -> foundCompanyEntities.add(assembler.toModel(company))));
        return foundCompanyEntities;
    }

    //get payment related to a special person
    @GetMapping("/companies/{id}/{personId}")
    public Payment getPaymentById(@PathVariable Long id, @PathVariable Long personId){
        Optional<Company> company = repository.findById(id);
        if (company.isPresent()){
            Payment payment = company.get().getPaymentById(personId);
            if (payment!=null)
                return payment;
            else
                throw new EntityNotFoundException(""+personId,"payment");
        }
        else{
            throw new EntityNotFoundException(""+id,"company");
        }

    }


    @PutMapping("/companies/{id}")
    ResponseEntity<?> update(@RequestBody Company newCompany  , @PathVariable Long id){
        Optional<Company> updateCompany = repository.findById(id).map(company -> {
            //check for not null ?
            company.setUserName(newCompany.getUserName());
            company.setEmail(newCompany.getEmail());
            company.setName(newCompany.getName());
            company.setPassword(newCompany.getPassword());
            company.setDescription(newCompany.getDescription());
            return repository.save(company);
        })/*TODO what should we do here? I dont even know*/;
        EntityModel<Company> entityModel = assembler.toModel(updateCompany.get());
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @DeleteMapping("/companies/{id}")
    ResponseEntity<?> delete(@PathVariable Long id){
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        throw new EntityNotFoundException(""+id,"company");
    }

}
